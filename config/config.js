const { config } = require('dotenv');

const { parsed: env } = config({ path: '.env.mysql' });

const { MYSQL_HOST, MYSQL_PORT, MYSQL_ROOT_PASSWORD } = process.env;

module.exports = {
  "development": {
    "username": "root",
    "password": MYSQL_ROOT_PASSWORD,
    "database": "dev_db",
    "host": MYSQL_HOST || "127.0.0.1",
    "port": MYSQL_PORT || 13306,
    "dialect": "mysql"
  },
  "test": {
    "username": "root",
    "password": MYSQL_ROOT_PASSWORD,
    "database": "test_db",
    "host": MYSQL_HOST || "127.0.0.1",
    "port": MYSQL_PORT || 13306,
    "dialect": "mysql"
  }
}
