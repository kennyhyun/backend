import request from 'supertest';

import HCard from '../../src/models/HCard';
import app from '../../src/app';

describe('post /submit', () => {
  it('create a card and set cardId in the cookie', async () => {
    const resp = await request(app)
      .post('/submit')
      .send(
        'givenName=Sam&surname=Fairfax&email=sam.fairfax%40fairfaxmedia.com.au&phone=0292822833&houseNumber=100&street=Harris+Street&suburb=Pyrmont&state=NSW&postcode=2009&country=Australia',
      );
    console.log(resp.body);
    expect(resp.headers).toHaveProperty('set-cookie');
    expect(resp.status).toBe(302);
    const card = await HCard.findOne({ order: [['createdAt', 'DESC']] });
    expect(card).toBeTruthy();
    expect(card).toEqual(
      expect.objectContaining({
        givenName: 'Sam',
        surname: 'Fairfax',
      }),
    );
  });
});
