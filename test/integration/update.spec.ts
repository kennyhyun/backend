import request from 'supertest';

import app from '../../src/app';
import HCard from '../../src/models/HCard';

describe('post /update', () => {
  it('do not update without hCardId in the cookie', async () => {
    const resp = await request(app).post('/update').send('suburb=Pyrmont');
    console.log(resp.body);
    expect(resp.status).toBe(200);
    expect(resp.body).toEqual({});
    const card = await HCard.findOne();
    expect(card).toBeFalsy();
  });
  it('update field with hCardId and return updated in the body', async () => {
    const card = await HCard.create({});
    const resp = await request(app)
      .post('/update')
      .set('Cookie', [`hCardId=${card.id}`])
      .send('suburb=Pyrmont');
    const expecting = expect.objectContaining({ suburb: 'Pyrmont' });
    expect(resp.body).toEqual(expecting);
    expect(resp.status).toBe(200);
    const saved = await HCard.findOne({ where: { id: card.id } });
    expect(saved).toEqual(expecting);
  });
});
