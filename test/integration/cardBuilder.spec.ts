import request from 'supertest';

import app from '../../src/app';
import { extractHCardProps } from '../../src/html';
import HCard from '../../src/models/HCard';
import { UnknownObject } from '../../src/types';

const getNoscriptBody = (html: string) => {
  const [, noscript] = html.match(/<noscript>[\r\n]*(.*)[\r\n]*<\/noscript>/) || [];
  return noscript;
};

describe('get /', () => {
  it('should have noscript rendered with hCardProps in script tag', async () => {
    const resp = await request(app).get('/');
    expect(resp.status).toBe(200);
    const { text: html } = resp;
    expect(html).toContain('<noscript');
    const noscript = getNoscriptBody(html);
    expect(noscript).toBeTruthy();
    const props = extractHCardProps(html);
    Object.values(props).forEach(value => {
      expect(noscript).toContain(value);
    });
  });

  it('should have noscript rendered with card props from database and update hCardProps if hCardId exist', async () => {
    const card = await HCard.create({ surname: 'Foobar' });
    const resp = await request(app)
      .get('/')
      .set('Cookie', [`hCardId=${card.id}`]);
    expect(resp.status).toBe(200);
    const { text: html } = resp;
    expect(html).toContain('<noscript');
    const noscript = getNoscriptBody(html);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { id, createdAt, deletedAt, updatedAt, ...props } = card.toJSON() as UnknownObject;
    Object.values(props).forEach(value => {
      expect(noscript).toContain(value);
    });
    const propsInJs = extractHCardProps(html);
    expect(propsInJs).toEqual(props);
    expect(card).toEqual(expect.objectContaining(props));
  });
});
