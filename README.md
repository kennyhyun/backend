# Backend test

- `/css`: static css files
- `/img`: static image files
- `/`, `/index.html`: html with server side rendering
- `/submit`: creates card with properties in the database and
  - redirect to `/`
  - sets hCardId of the created card in the cookie
- `/update`: update fields and return the updated document
  - saves only if hCardId was present in the cookie

Reloading after creating card will recall the created card in the form.

## Prerequisites

- node.js (12+)
- yarn
- docker

## Libraries used

- Typescript
- Sequelize
- Mysql
- Express

Docker Images

- mysql:5.7
- waisbrot/wait

## How to run

```shell
$ yarn
$ yarn start
```

and open http://localhost:3000 in the browser

To use a different port, use PORT env var

```shell
$ PORT=3001 yarn start
```

## How to run test

```shell
$ yarn
$ yarn test
```

## How to close mysql

test and dev env runs mysql docker container.

it's using port 13306.

if you have done with the dev, you can remove mysql by

```shell
$ yarn remove:mysql
```

or just stop for the future use

```shell
$ yarn stop:mysql
```

## Running prettier

You can setup your prettier in your editor but also run it with cli

```
$ yarn fix:prettier
```

It's recommended that all the files are already commited before run this because it will change the source codes.

## Trouble shooting

### Mysql port 13306

If you have already open the port, dev env will fail. please close the port before running this.

## TODO

This environment is not ready for the production yet.
