import express, { Handler, Request, Response, NextFunction } from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';

import { errorLogger } from './handlers/log';
import { errorHandler } from './handlers/error';
import { cardBuilder } from './handlers/cardBuilder';
import { createCard } from './handlers/createCard';
import { updateCard } from './handlers/updateCard';

const handlePromise = (handler: Handler) => async (req: Request, res: Response, next: NextFunction) => {
  try {
    await handler(req, res, next);
  } catch (e) {
    next(e);
  }
};

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.get('/', handlePromise(cardBuilder));
app.get('/index.html', handlePromise(cardBuilder));
app.post('/submit', handlePromise(createCard));
app.post('/update', handlePromise(updateCard));
app.use('/', express.static('public'));

app.use(errorLogger);
app.use(errorHandler);

export default app;
