import { config } from 'dotenv';

config({ path: '.env.mysql' });
config({ path: '.env' });

const {
  NODE_ENV,
  ROOT_ELEMENT_CLASS = 'HcardApp',
  PUBLIC_DIR = './public',
  INDEX_FILE = 'index.html',
  MYSQL_HOST = 'localhost',
  MYSQL_USER = 'root',
  MYSQL_PASSWORD = 'password',
  MYSQL_ROOT_PASSWORD = 'root_password',
  MYSQL_PORT = 13306,
  DB_DIALECT = 'mysql',
  DB_NAME: ENV_DB_NAME = 'dev_db',
} = process.env;

const isTest = NODE_ENV === 'test';

const DB_HOST = MYSQL_HOST;
const DB_USER = 'root';
const DB_PASSWD = MYSQL_ROOT_PASSWORD;
const DB_PORT = MYSQL_PORT;
const DB_NAME = isTest ? 'test_db' : ENV_DB_NAME;

export { ROOT_ELEMENT_CLASS, PUBLIC_DIR, INDEX_FILE, DB_NAME, DB_HOST, DB_USER, DB_PASSWD, DB_PORT, DB_DIALECT };
