import React from 'react';
import DomServer from 'react-dom/server';
import { UnknownObject } from './types';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
declare const global: any;
global.React = React;

export const renderHCardComponent = async (context: { [k: string]: UnknownObject }): Promise<string> => {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const HCardComponent = require('../public/main.js').default;
  return DomServer.renderToString(React.createElement(HCardComponent, context.hCardProps));
};
