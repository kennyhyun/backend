import { RequestHandler } from 'express';

import { HCard } from '../models';
import { UnknownObject } from '../types';

export const createCard: RequestHandler = async (req, res) => {
  const {
    body: {
      givenName,
      surname,
      email,
      phone,
      houseNumber,
      street,
      suburb,
      state,
      postcode,
      country,
    } = {} as UnknownObject,
  } = req;
  const hCard = await HCard.create({
    givenName,
    surname,
    email,
    phone,
    houseNumber,
    street,
    suburb,
    state,
    postcode,
    country,
  });
  res.cookie('hCardId', hCard.id, { maxAge: 900000 });
  res.redirect(302, '/');
};
