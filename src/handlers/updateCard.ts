import { RequestHandler } from 'express';

import { HCard } from '../models';
import { UnknownObject } from '../types';

export const updateCard: RequestHandler = async (req, res) => {
  const {
    cookies: { hCardId },
    body: {
      givenName,
      surname,
      email,
      phone,
      houseNumber,
      street,
      suburb,
      state,
      postcode,
      country,
    } = {} as UnknownObject,
  } = req;
  if (hCardId) {
    const [rowsUpdate] = await HCard.update(
      { givenName, surname, email, phone, houseNumber, street, suburb, state, postcode, country },
      { where: { id: hCardId } },
    );
    if (rowsUpdate === 1) {
      const updated = await HCard.findOne({ where: { id: hCardId } });
      res.json(updated);
    }
  }
  res.send();
};
