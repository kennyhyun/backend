import { RequestHandler } from 'express';
import { promises as fsp } from 'fs';
import path from 'path';

import { ROOT_ELEMENT_CLASS, PUBLIC_DIR, INDEX_FILE } from '../config';
import { renderHCardComponent } from '../ssr';
import { updateHCardProps, extractHCardProps } from '../html';
import { HCard } from '../models';

const rxRootElement = new RegExp(`(<[a-z-]+ class="${ROOT_ELEMENT_CLASS}" />)`);

export const cardBuilder: RequestHandler = async (req, res) => {
  const {
    cookies: { hCardId },
  } = req;
  const card = hCardId && (await HCard.findOne({ where: { id: hCardId } }));
  const hCardProps = card?.toJSON();
  let html = await fsp.readFile(path.join(PUBLIC_DIR, INDEX_FILE), 'utf8');
  const defaultHCardProps = extractHCardProps(html);
  const rendered = await renderHCardComponent({ hCardProps: hCardProps || defaultHCardProps });
  if (hCardProps) html = updateHCardProps(html, hCardProps);
  res.send(html.replace(rxRootElement, `<noscript>\n${rendered}\n</noscript>\n$1`));
};
