import { BuildOptions, Model, STRING, UUID, UUIDV4 } from 'sequelize';
import sequelize from '../db';

export class HCard extends Model {
  public id!: string;
  public givenName!: string | null;
  public surname!: string | null;
  public email!: string | null;
  public phone!: string | null;
  public houseNumber!: string | null;
  public street!: string | null;
  public suburb!: string | null;
  public state!: string | null;
  public country!: string | null;
  public postcode!: string | null;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public readonly deletedAt!: Date;
}

HCard.init(
  {
    id: {
      type: UUID,
      defaultValue: UUIDV4,
      allowNull: false,
      primaryKey: true,
    },
    givenName: STRING,
    surname: STRING,
    email: STRING,
    phone: STRING,
    houseNumber: STRING,
    street: STRING,
    suburb: STRING,
    state: STRING,
    country: STRING,
    postcode: STRING,
  },
  {
    sequelize,
    modelName: 'hcard',
    timestamps: true,
    paranoid: true,
  },
);

export type HCardModelStatic = typeof Model & {
  new (values?: Record<string, unknown>, options?: BuildOptions): HCard;
};

export default HCard as HCardModelStatic;
