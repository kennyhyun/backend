export type UnknownObject = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [k: string]: any;
};
