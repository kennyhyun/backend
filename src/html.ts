import { UnknownObject } from './types';
const rxHCardPropsBody = /hCardProps = ({(.|[\n\r])*});/;

export const updateHCardProps = (html: string, hCardProps: UnknownObject): string => {
  const props = Object.entries(hCardProps).reduce((obj, [k, v]) => {
    if (['createdAt', 'updatedAt', 'deletedAt', 'id'].includes(k)) return obj;
    if (v === null) return obj;
    return { ...obj, [k]: v };
  }, {} as UnknownObject);
  return html.replace(rxHCardPropsBody, `hCardProps = ${JSON.stringify(props, null, 2)};`);
};

export const extractHCardProps = (html: string): UnknownObject => {
  const [, hCardPropsString] = html.match(rxHCardPropsBody) || [];
  if (!hCardPropsString) return {};
  try {
    return JSON.parse(eval(`JSON.stringify(${hCardPropsString})`));
  } catch (e) {
    console.warn(e);
    return {};
  }
};
