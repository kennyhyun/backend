import { Sequelize } from 'sequelize';
import * as config from './config';

const { DB_NAME, DB_USER, DB_PASSWD, DB_HOST, DB_PORT, DB_DIALECT } = config;

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASSWD, {
  host: DB_HOST,
  port: Number(DB_PORT || 3306),
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  dialect: DB_DIALECT as any,
});

if (process.env.NODE_ENV !== 'test') {
  sequelize.sync();
}

export default sequelize;
