module.exports = {
  testEnvironment: 'node',
  transform: {
    '^.+\\.ts?$': 'ts-jest',
  },
  setupFilesAfterEnv: ['./test/testSetup.ts'],
  collectCoverage: true,
  maxWorkers: 1,
};
